# -*- coding: utf-8 -*-
from actors.actors import *
import argparse
import json
from program_state.redis_interface import *
from program_state.redis_config import *
from session_config import sessions

SUPPORTED_ACTIONS = [
    "state_construct_empty_states_P",
    "state_append_concrete_ip_constraint_P",
    "state_append_concrete_sp_constraint_P",
    "state_append_concrete_sp_mem_constraint_P",
    "state_append_concrete_reg_constraint_P",
    "state_append_concrete_mem_constraint_P",
    "state_append_concrete_reg_mem_constraint_P",
    "state_merge_concrete_sp_constraint_P",
    "state_merge_concrete_sp_mem_constraint_P",
    "state_merge_concrete_reg_constraint_P",
    "state_merge_concrete_mem_constraint_P",
    "state_merge_concrete_reg_mem_constraint_P",
    "state_append_controllable_ip_constraint_P",
    "state_append_controllable_sp_constraint_P",
    "state_append_controllable_sp_mem_constraint_P",
    "state_append_controllable_reg_constraint_P",
    "state_append_controllable_mem_constraint_P",
    "state_append_controllable_reg_mem_constraint_P",
    "state_merge_controllable_sp_mem_constraint_P",
    "state_merge_controllable_reg_constraint_P",
    "state_merge_controllable_mem_constraint_P",
    "state_merge_controllable_reg_mem_constraint_P"
]


class StateModifier:
    def __init__(self, action_name, session_id, old_states, arguments, is_arch32):
        self._action_name = action_name
        self._session_id = session_id
        self._old_states = old_states
        self._arguments = arguments
        self._is_arch32 = is_arch32

    def get_new_states(self):
        new_states = eval(self._action_name)(self._old_states, self._arguments[1:-1], self._is_arch32)
        return new_states


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="输入的谓词信息，json格式")
    parser.add_argument("-f", "--file", help="采用文件的形式输入谓词数据，json格式")

    redis_client = ProgramStateRedisClient(server_host=REDIS_HOST, server_port=REDIS_PORT, server_password=REDIS_PASS)
    redis_client.connect()

    args = parser.parse_args()

    if args.input:
        input_json = json.loads(args.input)
        session_id = input_json['session_id']
        is_arch32 = sessions[session_id]['is_arch32']
        query_info = input_json['query']
        action_name = query_info['query_name']
        args = query_info['args']
        if True:  # Who knows why? I don't anyway.
            comments_info = args[0]
            old_states_info = args[1]
            args[0] = old_states_info
            args[1] = comments_info

        if action_name not in SUPPORTED_ACTIONS:
            print(json.dumps( {
                'processed': 0,
                'res': {"status": 0, "info": "ERROR", "data": {"Invalid query_name!"}}
            }))
            return

        if True: #'index' in input_json:
            if False: #input_json['index'] > 0:
                print(json.dumps({
                    'processed': 0,
                    'res': {"status": 1, "info": "ok", "data": {"valid": 0, "args": []}}
                }))
                return
            else:
                if action_name != "state_construct_empty_states_P":
                    old_states_hash_list = args[0]['value']
                    old_states = redis_client.recover_state_chain_by_hash_list(old_states_hash_list, session_id)
                else:
                    old_states_hash_list = []
                    old_states = None

                if old_states is None:
                    old_states = StateChain()
                SM = StateModifier(action_name=action_name, session_id=session_id, old_states=old_states, arguments=args, is_arch32=is_arch32)
                new_states = SM.get_new_states()
                new_states_hash_list = []
                for index in range(new_states.num_states):
                    state = new_states.get_state_by_index(index)
                    state_hash = state.get_md5_hash()
                    new_states_hash_list.append(state_hash)
                    if state_hash not in old_states_hash_list:
                        redis_client.add_program_state(state, session_id)

                args = args[:-1]
                args.append({'type': 'json', 'value': new_states_hash_list, 'concrete': 1})

        print(json.dumps({
            'processed': 1,
            'res':  {"status": 1, "info": "ok", "data": {"valid": 1, "args": args}}
        }))


if __name__ == "__main__":
    main()
