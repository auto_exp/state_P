sessions = {
        "test_semantics": {'is_arch32': False, 'target_program': 'ffmpeg'},
        "test_2020-13160": {'is_arch32': False, 'target_program': 'anydesk'},
        "test_2017_9430": {'is_arch32': True, 'target_program': 'dnstracer'},
        "test_2017_13089": {'is_arch32': False, 'target_program': 'wget'},
        "test_2019_12527": {'is_arch32': False, 'target_program': 'squid'}
}
